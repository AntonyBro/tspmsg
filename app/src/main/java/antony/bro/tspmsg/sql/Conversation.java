package antony.bro.tspmsg.sql;

public class Conversation {
    private long mId;
    private String mName;
    private long mContactId;

    public Conversation(long id, String name, long contactId) {
        this.mId = id;
        this.mName = name;
        this.mContactId = contactId;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public long getContactId() {
        return mContactId;
    }

    public void setContactId(long contactId) {
        this.mContactId = contactId;
    }
}
