package antony.bro.tspmsg.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SQLiteCursorWrapper {

    private static SQLiteCursorWrapper mInstance;
    private final SQLiteDatabase mDatabase;
    private Cursor mCursor;
    private Contact mCurrentProfile;

    public static synchronized SQLiteCursorWrapper getInstance(Context context) {
        if (mInstance == null) {
            Log.wtf("getInstance", "Open DB");
            mInstance = new SQLiteCursorWrapper(context.getApplicationContext());
        }
        return mInstance;
    }

    private SQLiteCursorWrapper(Context context) {
        SQLiteOpenHelper mSqLiteOpenHelper = new SQLiteDatabaseHelper(context);
        mDatabase = mSqLiteOpenHelper.getReadableDatabase();
    }

    public void closeDB() {
        Log.wtf("closeDB", "Close DB");
        mCursor.close();
        mDatabase.close();
    }

    public Contact getMyProfile() {
        Contact contact = null;

        try {
            mCursor = mDatabase.query("PROFILES",
                    new String[]{"_id", "EMAIL", "NAME"},
                    "CURRENT = ?", new String[]{"1"}, null, null, null);

            if (mCursor.moveToFirst()) {
                contact = new Contact(
                        mCursor.getLong(0),
                        mCursor.getString(1),
                        mCursor.getString(2));
            }
        } catch (SQLiteException e) {
            Log.wtf("getMyProfile", "SQLiteException");
        }
        mCurrentProfile = contact;
        return contact;
    }

    public void logoutProfile() {
        ContentValues currentValue = new ContentValues();
        currentValue.put("CURRENT", 0);
        mDatabase.update("PROFILES", currentValue, "CURRENT = ?", new String[]{"1"});
    }

    public List<Contact> getAllContacts() {
        List<Contact> contacts = null;

        try {
            mCursor = mDatabase.query("CONTACTS",
                    new String[]{"_id", "EMAIL", "NAME"},
                    "PROFILE_ID = ?", new String[] {Long.toString(mCurrentProfile.getId())}, null, null, null);

            if (mCursor.moveToFirst()) {
                contacts = new ArrayList<>();
                do {
                    Contact contact = new Contact(
                            mCursor.getLong(0),
                            mCursor.getString(1),
                            mCursor.getString(2));

                    contacts.add(contact);
                } while (mCursor.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.i("getAllContacts", "SQLiteException");
        }

        return contacts;
    }

    public List<Conversation> getAllConversations() {
        List<Conversation> conversations = null;

        try {
            mCursor = mDatabase.query("CONVERSATION",
                    new String[]{"_id", "NAME", "CONTACT_ID"},
                    "PROFILE_ID = ?", new String[] {Long.toString(mCurrentProfile.getId())}, null, null, null);

            if (mCursor.moveToFirst()) {
                conversations = new ArrayList<>();
                do {
                    Conversation conversation = new Conversation(
                            mCursor.getLong(0),
                            mCursor.getString(1),
                            mCursor.getInt(2));

                    conversations.add(conversation);
                } while (mCursor.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.i("getAllConversations", "SQLiteException");
        }

        return conversations;
    }

    public List<Message> getAllMessages(Long conversationId) {
        List<Message> messages = null;

        try {
            mCursor = mDatabase.query("MESSAGES",
                    new String[]{"_id", "ME", "MESSAGE", "DATE", "CONVERSATION_ID"},
                    "CONVERSATION_ID = ?", new String[]{conversationId.toString()}, null, null, null);

            if (mCursor.moveToFirst()) {
                messages = new ArrayList<>();
                do {
                    Message message = new Message(
                            mCursor.getLong(0),
                            mCursor.getInt(1) == 1,
                            mCursor.getString(2),
                            mCursor.getString(3),
                            mCursor.getLong(4));

                    messages.add(message);
                } while (mCursor.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.i("getAllMessages", "SQLiteException");
        }

        return messages;
    }

    public Message getLastMessage(Conversation conversation) {
        Message message = null;

        try {
            mCursor = mDatabase.query("MESSAGES",
                    new String[]{"MAX(_id)", "ME", "MESSAGE", "DATE", "CONVERSATION_ID"},
                    "CONVERSATION_ID = ?", new String[]{Long.toString(conversation.getId())}, null, null, null);

            if (mCursor.moveToFirst()) {
                message = new Message(
                        mCursor.getLong(0),
                        mCursor.getInt(1) == 1,
                        mCursor.getString(2),
                        mCursor.getString(3),
                        mCursor.getLong(4));
            }
        } catch (SQLiteException e) {
            Log.i("getLastMessage", "SQLiteException");
        }

        return message;
    }

    public Contact getContactByConversationId(long id) {
        Contact contact = null;

        try {
            mCursor = mDatabase.query("CONVERSATION",
                    new String[]{"CONTACT_ID"},
                    "_id = ?", new String[]{Long.toString(id)}, null, null, null);

            if (mCursor.moveToFirst()) {
                Long contactId = mCursor.getLong(0);

                mCursor = mDatabase.query("CONTACTS",
                        new String[]{"_id", "EMAIL", "NAME"},
                        "_id = ?", new String[]{contactId.toString()}, null, null, null);

                if (mCursor.moveToFirst()) {
                    contact = new Contact(
                            mCursor.getLong(0),
                            mCursor.getString(1),
                            mCursor.getString(2));
                }
            }
        } catch (SQLiteException e) {
            Log.wtf("getContactByConversationId", "SQLiteException");
        }

        return contact;
    }

    public void insertProfile(String email, String name) {
        try {
            mCursor = mDatabase.query("PROFILES",
                    new String[]{"_id", "EMAIL", "NAME", "CURRENT"},
                    "EMAIL = ?", new String[]{email}, null, null, null);

            if (mCursor.moveToFirst()) {
                ContentValues updateValue = new ContentValues();
                updateValue.put("CURRENT", 0);
                mDatabase.update("PROFILES", updateValue, "CURRENT = ?", new String[]{"1"});

                ContentValues currentValue = new ContentValues();
                currentValue.put("CURRENT", 1);
                mDatabase.update("PROFILES", currentValue, "EMAIL = ?", new String[]{email});
            } else {
                ContentValues updateValue = new ContentValues();
                updateValue.put("CURRENT", 0);
                mDatabase.update("PROFILES", updateValue, "CURRENT = ?", new String[]{"1"});

                ContentValues contactValue = new ContentValues();
                contactValue.put("NAME", name);
                contactValue.put("EMAIL", email);
                contactValue.put("CURRENT", 1);
                mDatabase.insert("PROFILES", null, contactValue);
            }
        } catch (SQLiteException e) {
            Log.i("insertProfile", "SQLiteException");
        }
    }

    public void insertContact(String email, String name) {
        ContentValues contactValue = new ContentValues();
        contactValue.put("EMAIL", email);
        contactValue.put("NAME", name);
        contactValue.put("PROFILE_ID", mCurrentProfile.getId());
        mDatabase.insert("CONTACTS", null, contactValue);
    }

    public long createConversation(String name, long contactId) {
        ContentValues conversationValues = new ContentValues();
        conversationValues.put("NAME", name);
        conversationValues.put("CONTACT_ID", contactId);
        conversationValues.put("PROFILE_ID", mCurrentProfile.getId());

        return mDatabase.insert("CONVERSATION", null, conversationValues);
    }

    public void insertMessage(int isMe, String message, String date, long conversationId) {
        ContentValues messageValues = new ContentValues();
        messageValues.put("ME", isMe);
        messageValues.put("MESSAGE", message);
        messageValues.put("DATE", date);
        messageValues.put("CONVERSATION_ID", conversationId);
        mDatabase.insert("MESSAGES", null, messageValues);
    }

    public void deleteContact(long contactId) {
        mDatabase.delete("CONTACTS", "_id = ?",
                new String[]{Long.toString(contactId)});
    }

    public void deleteConversation(long conversationId) {
        mDatabase.delete("CONVERSATION", "_id = ?",
                new String[]{Long.toString(conversationId)});
    }

    public void deleteMessage(long messageId) {
        mDatabase.delete("MESSAGES", "_id = ?",
                new String[]{Long.toString(messageId)});
    }

    public void deleteAllMessages(long conversationId) {
        mDatabase.delete("MESSAGES", "CONVERSATION_ID = ?",
                new String[]{Long.toString(conversationId)});
    }
}
