package antony.bro.tspmsg.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class SQLiteDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "TSPMessenger";
    private static final int DB_VERSION = 1;

    public SQLiteDatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {

            db.execSQL("CREATE TABLE PROFILES (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "EMAIL TEXT,"
                    + "NAME TEXT,"
                    + "CURRENT INTEGER);");

            db.execSQL("CREATE TABLE CONTACTS (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "EMAIL TEXT,"
                    + "NAME TEXT,"
                    + "PROFILE_ID INTEGER);");

            db.execSQL("CREATE TABLE CONVERSATION (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT,"
                    + "CONTACT_ID INTEGER,"
                    + "PROFILE_ID INTEGER);");

            db.execSQL("CREATE TABLE MESSAGES (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "ME INTEGER,"
                    + "MESSAGE TEXT,"
                    + "DATE TEXT,"
                    + "CONVERSATION_ID INTEGER);");
        }
    }
}
