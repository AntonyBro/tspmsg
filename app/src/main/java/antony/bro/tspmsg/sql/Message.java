package antony.bro.tspmsg.sql;

public class Message {
    private long mId;
    private boolean mIsMe;
    private String mMessage;
    private String mDateTime;
    private long mConversationId;

    public Message() {
    }

    public Message(boolean isMe, String message, String dateTime, long conversationId) {
        this.mIsMe = isMe;
        this.mMessage = message;
        this.mDateTime = dateTime;
        this.mConversationId = conversationId;
    }

    public Message(long id, boolean isMe, String message, String dateTime, long conversationId) {
        this.mId = id;
        this.mIsMe = isMe;
        this.mMessage = message;
        this.mDateTime = dateTime;
        this.mConversationId = conversationId;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public int isMe() {
        return mIsMe ? 1 : 0;
    }

    public void setIsMe(boolean isMe) {
        this.mIsMe = isMe;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public void setDateTime(String dateTime) {
        this.mDateTime = dateTime;
    }

    public long getConversationId() {
        return mConversationId;
    }

    public void setConversationId(long conversationId) {
        this.mConversationId = conversationId;
    }
}
