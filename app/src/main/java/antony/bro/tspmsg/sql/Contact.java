package antony.bro.tspmsg.sql;

public class Contact {
    private long mId;
    private String mEmail;
    private String mName;

    public Contact(long id, String email, String name) {
        this.mId = id;
        this.mEmail = email;
        this.mName = name;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
