package antony.bro.tspmsg;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class AsyncTaskURL extends AsyncTask<Void, Void, String> {
    public interface AsyncResponseHandler {

        void processFinish(String json);
    }

    private AsyncResponseHandler mAsyncResponseHandler = null;
    private String mUrl;

    public AsyncTaskURL() {
    }

    public AsyncTaskURL(String url) {
        mUrl = url;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public void setAsyncResponseHandler(AsyncResponseHandler asyncResponseHandler) {
        this.mAsyncResponseHandler = asyncResponseHandler;
    }

    @Override
    protected String doInBackground(Void... params) {
        URL url = null;

        try {
            url = new URL(mUrl);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        URLConnection urlConnection = null;
        StringBuilder json = new StringBuilder();

        try {
            if (url != null) {
                urlConnection = url.openConnection();
            }
            InputStream in = null;
            if (urlConnection != null) {
                in = new BufferedInputStream(urlConnection.getInputStream());
            }

            BufferedReader reader = null;
            if (in != null) {
                reader = new BufferedReader(new InputStreamReader(in));
            }

            String line;
            if (reader != null) {
                while ((line = reader.readLine()) != null) {
                    json.append(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json.toString();
    }

    @Override
    protected void onPostExecute(String json) {
        mAsyncResponseHandler.processFinish(json);
    }
}
