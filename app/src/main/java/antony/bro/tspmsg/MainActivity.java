package antony.bro.tspmsg;

import android.app.Activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import antony.bro.tspmsg.fragments.AddConversationFragment;
import antony.bro.tspmsg.fragments.ContactsFragment;
import antony.bro.tspmsg.fragments.ConversationsFragment;
import antony.bro.tspmsg.fragments.SettingsFragment;
import antony.bro.tspmsg.sql.Contact;
import antony.bro.tspmsg.sql.SQLiteCursorWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout = null;
    private String mName;
    private String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Contact contact = SQLiteCursorWrapper.getInstance(this).getMyProfile();
        if (contact != null) {
            mName = contact.getName();
            mEmail = contact.getEmail();
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);

            return;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (mDrawerLayout != null) {
            mDrawerLayout.addDrawerListener(toggle);
        }
        toggle.syncState();

        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if (getCurrentFocus() != null)
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }

        selectDrawerItem(R.id.nav_messages);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        TextView profileName = (TextView) findViewById(R.id.profile_name);
        TextView profileEmail = (TextView) findViewById(R.id.profile_email);

        if (profileEmail != null && profileName != null) {
            profileName.setText(mName);
            profileEmail.setText(mEmail);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        selectDrawerItem(item.getItemId());

        return true;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START))
            mDrawerLayout.closeDrawers();
        else {
            if (fm.getBackStackEntryCount() > 1) {
                if (fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName().equals(AddConversationFragment.EXTRA_ADD_CONVERSATION)) {
                    fm.popBackStack();
                } else {
                    fm.popBackStack(ConversationsFragment.EXTRA_CONVERSATIONS_FRAGMENT, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            } else {
                super.onBackPressed();
            }
        }
    }

    private void selectDrawerItem(int id) {
        Fragment fragment;
        String title;

        switch (id) {
            case R.id.nav_messages:
                fragment = new ConversationsFragment();
                title = getResources().getString(R.string.app_name);
                break;
            case R.id.nav_contacts:
                fragment = new ContactsFragment();
                title = getResources().getString(R.string.contacts);
                break;
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                title = getResources().getString(R.string.settings);
                break;
            case R.id.nav_logout:
                logout();
                return;

            default:
                fragment = new ConversationsFragment();
                title = getResources().getString(R.string.app_name);
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    private void logout() {
        SQLiteCursorWrapper.getInstance(this).logoutProfile();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
