package antony.bro.tspmsg.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import antony.bro.tspmsg.R;

public class BottomDialogOptions extends Dialog {

    private TextView mClearHistory;
    private TextView mDelete;
    private final boolean mDialogState;

    public BottomDialogOptions(Context context) {
        super(context);
        this.mDialogState = false;
    }

    public BottomDialogOptions(Context context, boolean isContactDialog) {
        super(context);
        this.mDialogState = isContactDialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (mDialogState) {
            setContentView(R.layout.dialog_contacts_options);
            mDelete = (TextView) findViewById(R.id.delete_contacts);
        } else {
            setContentView(R.layout.dialog_conversation_options);
            mClearHistory = (TextView) findViewById(R.id.clear_history);
            mDelete = (TextView) findViewById(R.id.delete_conversation);
        }

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(params);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }

    public TextView getClearHistory() {
        return mClearHistory;
    }

    public TextView getDelete() {
        return mDelete;
    }
}
