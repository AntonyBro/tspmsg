package antony.bro.tspmsg.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
    private ItemClickListener mListener;

    public BaseViewHolder(View itemView) {
        super(itemView);
        itemView.setTag(itemView);
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public BaseViewHolder(CardView itemView) {
        super(itemView);
        itemView.setTag(itemView);
        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void setItemClickListener(ItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View v) {
        mListener.onClick(v, getLayoutPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {
        mListener.onClick(v, getLayoutPosition(), true);
        return true;
    }
}
