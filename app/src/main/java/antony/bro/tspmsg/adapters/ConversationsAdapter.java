package antony.bro.tspmsg.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import antony.bro.tspmsg.R;
import antony.bro.tspmsg.sql.Conversation;
import antony.bro.tspmsg.sql.Message;
import antony.bro.tspmsg.sql.SQLiteCursorWrapper;

import java.util.ArrayList;
import java.util.List;

public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.ViewHolder> {
    private ItemClickListener mItemClickListener;
    private final List<Conversation> mConversations;
    private final Context mContext;
    private final List<TextView> mLastMessagesView;

    public ConversationsAdapter(List<Conversation> conversations, Context context) {
        this.mConversations = conversations;
        this.mContext = context;
        this.mLastMessagesView = new ArrayList<>();
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cardView = (CardView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_conversation, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(mConversations.get(position).getName());
        Message lastMessage = SQLiteCursorWrapper.getInstance(mContext).getLastMessage(mConversations.get(position));
        holder.lastMessage.setText(lastMessage.getMessage());

        mLastMessagesView.add(position, holder.lastMessage);

        holder.setItemClickListener(mItemClickListener);
    }

    @Override
    public int getItemCount() {
        return mConversations.size();
    }

    public void delete(int position) {
        mConversations.remove(position);
        notifyItemRemoved(position);
    }

    public void clearLastMessage(int position) {
        mLastMessagesView.get(position).setText("");
    }

    public static class ViewHolder extends BaseViewHolder {
        public final TextView name;
        public final TextView lastMessage;
        public final ImageView conversationPhoto;

        public ViewHolder(CardView itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.conversation_name);
            lastMessage = (TextView) itemView.findViewById(R.id.lastMessage);
            conversationPhoto = (ImageView) itemView.findViewById(R.id.photo);
        }
    }
}