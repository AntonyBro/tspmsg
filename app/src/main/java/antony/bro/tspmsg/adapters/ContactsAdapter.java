package antony.bro.tspmsg.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import antony.bro.tspmsg.R;
import antony.bro.tspmsg.sql.Contact;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {
    private final List<Contact> mContacts;
    private ItemClickListener mItemClickListener;

    public ContactsAdapter(List<Contact> contacts) {
        this.mContacts = contacts;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cardView = (CardView)LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_item_contact, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.name.setText(mContacts.get(position).getName());
        holder.email.setText(mContacts.get(position).getEmail());

        holder.setItemClickListener(mItemClickListener);
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public void delete(int position) {
        mContacts.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends BaseViewHolder {
        public final TextView name;
        public final TextView email;
        public final ImageView photo;

        public ViewHolder(CardView itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.contact_name);
            email = (TextView) itemView.findViewById(R.id.email);
            photo = (ImageView) itemView.findViewById(R.id.photo);
        }
    }
}