package antony.bro.tspmsg.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import antony.bro.tspmsg.R;
import antony.bro.tspmsg.sql.Message;

import java.util.List;

public class MessageAdapter extends BaseAdapter {

    private final List<Message> mChatMessages;
    private final Activity mContext;
    private ItemClickListener mItemClickListener;

    public MessageAdapter(Activity context, List<Message> chatMessages) {
        this.mContext = context;
        this.mChatMessages = chatMessages;
    }

    @Override
    public int getCount() {
        if (mChatMessages != null)
            return mChatMessages.size();
        else
            return 0;
    }

    @Override
    public Message getItem(int position) {
        if (mChatMessages != null)
            return mChatMessages.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Message chatMessage = getItem(position);
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.single_item_message, null);
            holder = createViewHolder(convertView);
            holder.contentWithBG.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onClick(v, 0, false);
                }
            });
            holder.contentWithBG.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mItemClickListener.onClick(v, 0, true);
                    return true;
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        setAlignment(holder, chatMessage.isMe() == 1);
        holder.textMessage.setText(chatMessage.getMessage());
        holder.dateMessage.setText(chatMessage.getDateTime());

        return convertView;
    }

    public void add(Message message) {
        mChatMessages.add(message);
    }

    public void add(List<Message> messages) {
        mChatMessages.addAll(messages);
    }

    private void setAlignment(ViewHolder holder, boolean isMe) {
        if (isMe) {
            holder.contentWithBG.setBackgroundResource(R.drawable.msg_out);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.textMessage.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.textMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.dateMessage.getLayoutParams();
            layoutParams.gravity = Gravity.RIGHT;
            holder.dateMessage.setLayoutParams(layoutParams);
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.msg_in);

            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) holder.contentWithBG.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
            layoutParams = (LinearLayout.LayoutParams) holder.textMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.textMessage.setLayoutParams(layoutParams);

            layoutParams = (LinearLayout.LayoutParams) holder.dateMessage.getLayoutParams();
            layoutParams.gravity = Gravity.LEFT;
            holder.dateMessage.setLayoutParams(layoutParams);
        }
    }

    private ViewHolder createViewHolder(View v) {
        ViewHolder holder = new ViewHolder();
        holder.textMessage = (TextView) v.findViewById(R.id.txtMessage);
        holder.content = (LinearLayout) v.findViewById(R.id.content);
        holder.contentWithBG = (LinearLayout) v.findViewById(R.id.contentWithBackground);
        holder.dateMessage = (TextView) v.findViewById(R.id.dateMessage);
        return holder;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;
    }

    private static class ViewHolder {
        public TextView textMessage;
        public TextView dateMessage;
        public LinearLayout content;
        public LinearLayout contentWithBG;
    }
}