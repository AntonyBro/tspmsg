package antony.bro.tspmsg.fragments;


import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import antony.bro.tspmsg.MainActivity;
import antony.bro.tspmsg.R;
import antony.bro.tspmsg.adapters.ConversationsAdapter;
import antony.bro.tspmsg.adapters.ItemClickListener;
import antony.bro.tspmsg.dialogs.BottomDialogOptions;
import antony.bro.tspmsg.sql.Conversation;
import antony.bro.tspmsg.sql.SQLiteCursorWrapper;

import java.util.List;

public class ConversationsFragment extends Fragment {

    public static final String EXTRA_CONVERSATIONS_FRAGMENT = "conversations_fragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversations, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.conversations);

        View view = getView();
        if (view != null) {

            final List<Conversation> conversations = SQLiteCursorWrapper.getInstance(getActivity()).getAllConversations();

            FloatingActionButton addMessageBtn = (FloatingActionButton) view.findViewById(R.id.add_conversation);
            if (addMessageBtn != null) {
                addMessageBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        doFragmentTransaction(new AddConversationFragment());
                    }
                });
            }

            if (conversations == null)
                return;

            final RecyclerView conversationsList = (RecyclerView) view.findViewById(R.id.messages_list);
            conversationsList.setLayoutManager(new LinearLayoutManager(getActivity()));

            final ConversationsAdapter conversationsAdapter = new ConversationsAdapter(conversations, getActivity());
            conversationsAdapter.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, final int position, boolean isLongClick) {
                    if (!isLongClick) {
                        Bundle bundle = new Bundle();
                        bundle.putLong(MessageFragment.EXTRA_CONVERSATION_ID, conversations.get(position).getId());

                        MessageFragment fragment = new MessageFragment();
                        fragment.setArguments(bundle);

                        doFragmentTransaction(fragment);

                        ((MainActivity) getActivity()).getSupportActionBar().setTitle(conversations.get(position).getName());
                    } else {
                        final BottomDialogOptions dialog = new BottomDialogOptions(getActivity());
                        dialog.show();

                        dialog.getClearHistory().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SQLiteCursorWrapper.getInstance(getActivity()).deleteAllMessages(conversations.get(position).getId());

                                conversationsAdapter.clearLastMessage(position);

                                dialog.dismiss();
                            }
                        });

                        dialog.getDelete().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SQLiteCursorWrapper.getInstance(getActivity()).deleteConversation(conversations.get(position).getId());
                                SQLiteCursorWrapper.getInstance(getActivity()).deleteAllMessages(conversations.get(position).getId());

                                conversationsAdapter.delete(position);

                                dialog.dismiss();
                            }
                        });
                    }
                }
            });

            conversationsList.setAdapter(conversationsAdapter);
        }
    }

    private void doFragmentTransaction(Fragment fragment) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                R.anim.enter_from_left, R.anim.exit_to_right);
        ft.replace(R.id.content_frame, fragment);
        ft.addToBackStack(EXTRA_CONVERSATIONS_FRAGMENT);
        ft.commit();
    }
}