package antony.bro.tspmsg.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import antony.bro.tspmsg.AsyncTaskURL;
import antony.bro.tspmsg.MainActivity;
import antony.bro.tspmsg.R;
import antony.bro.tspmsg.sql.SQLiteCursorWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginFragment extends Fragment {

    private EditText mName;
    private EditText mEmail;
    private EditText mPassword;
    private Button mLoginBtn;
    private TextView mMode;
    private InputMethodManager mInputMethodManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        View view = getView();
        if (view != null) {
            mName = (EditText) view.findViewById(R.id.name);
            mEmail = (EditText) view.findViewById(R.id.email);
            mPassword = (EditText) view.findViewById(R.id.password);
            mLoginBtn = (Button) view.findViewById(R.id.login_btn);
            mMode = (TextView) view.findViewById(R.id.mode);
            mInputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            if (mLoginBtn.getText().toString().equals(getString(R.string.login)))
                mName.setVisibility(View.GONE);

            mLoginBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    attemptLogin();
                }
            });

            mMode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeView();
                }
            });
        }
    }

    private void changeView() {
        if (mMode.getText().toString().equals(getString(R.string.registration))) {
            mName.setVisibility(View.VISIBLE);
            mName.requestFocus();
            mInputMethodManager.showSoftInput(mName, InputMethodManager.SHOW_IMPLICIT);
            mLoginBtn.setText(getString(R.string.registration));
            mMode.setText(getString(R.string.login));
        } else {
            mName.setVisibility(View.GONE);
            mMode.setText(getString(R.string.registration));
            mLoginBtn.setText(getString(R.string.login));
        }
    }

    private void attemptLogin() {
        boolean isEmailValid;
        boolean isPasswordValid;

        if (!isEmailValid(mEmail.getText().toString())) {
            mEmail.setError(getString(R.string.invalid_email));
            isEmailValid = false;
        } else
            isEmailValid = true;

        if (mPassword.getText().toString().length() < 4) {
            mPassword.setError(getString(R.string.invalid_pass));
            isPasswordValid = false;
        } else
            isPasswordValid = true;

        if (mName.getText().toString().length() < 3)
            mName.setError(getString(R.string.invalid_name));

        boolean isLogin = mLoginBtn.getText() == getString(R.string.login);

        if (isEmailValid && isPasswordValid) {
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            AsyncTaskURL asyncTaskURL = new AsyncTaskURL();

            if (isLogin)
                asyncTaskURL.setUrl("http://128.199.52.224:9000/login?email=" + mEmail.getText().toString()
                        + "&password=" + mPassword.getText().toString());
            else
                asyncTaskURL.setUrl("http://128.199.52.224:9000/registration?name=" + mName.getText().toString()
                        + "&email=" + mEmail.getText().toString()
                        + "&password=" + mPassword.getText().toString());

            asyncTaskURL.setAsyncResponseHandler(new AsyncTaskURL.AsyncResponseHandler() {
                @Override
                public void processFinish(String json) {
                    Log.wtf("login: ", json);
                    progressDialog.hide();
                    switch (json) {
                        case "login":
                            mEmail.setError(getString(R.string.incorrect_email_pass));
                            mPassword.setError(getString(R.string.incorrect_email_pass));
                            mInputMethodManager.showSoftInput(mEmail, InputMethodManager.SHOW_IMPLICIT);

                            break;
                        case "registration":
                            mEmail.setError(getString(R.string.email_is_registered));
                            mInputMethodManager.showSoftInput(mEmail, InputMethodManager.SHOW_IMPLICIT);

                            break;
                        default:
                            try {
                                JSONObject jsonObject = new JSONObject(json);

                                SQLiteCursorWrapper.getInstance(getActivity()).insertProfile(
                                        jsonObject.getString("Email"),
                                        jsonObject.getString("Name"));

                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            break;
                    }
                }
            });

            asyncTaskURL.execute((Void) null);

            mInputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    private boolean isEmailValid(String email) {
        String regularExpression =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@" +
                "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" +
                "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\." +
                "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?" +
                "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|" +
                "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        Pattern pattern = Pattern.compile(regularExpression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        return matcher.matches();
    }
}
