package antony.bro.tspmsg.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import antony.bro.tspmsg.MainActivity;
import antony.bro.tspmsg.R;
import antony.bro.tspmsg.adapters.ContactsAdapter;
import antony.bro.tspmsg.adapters.ItemClickListener;
import antony.bro.tspmsg.dialogs.BottomDialogOptions;
import antony.bro.tspmsg.sql.Contact;
import antony.bro.tspmsg.sql.SQLiteCursorWrapper;

import java.util.List;

public class ContactsFragment extends Fragment {

    public static final String EXTRA_NAME = "name";
    public static final String EXTRA_EMAIL = "EMAIL";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contacts, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.contacts);

        View view = getView();
        if (view != null) {
            if (getArguments() != null)
                SQLiteCursorWrapper.getInstance(getActivity()).insertContact(
                        getArguments().getString(EXTRA_EMAIL),
                        getArguments().getString(EXTRA_NAME)
                );

            FloatingActionButton addMessageBtn = (FloatingActionButton) view.findViewById(R.id.add_contact);
            if (addMessageBtn != null) {
                addMessageBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        SearchFragment fragment = new SearchFragment();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                                R.anim.enter_from_left, R.anim.exit_to_right);
                        ft.replace(R.id.content_frame, fragment);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
            }

            final List<Contact> contacts = SQLiteCursorWrapper.getInstance(getActivity()).getAllContacts();

            if (contacts == null)
                return;

            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);

            final RecyclerView contactsRecyclerView = (RecyclerView) view.findViewById(R.id.contacts_list);
            contactsRecyclerView.setLayoutManager(layoutManager);

            final ContactsAdapter contactsListAdapter = new ContactsAdapter(contacts);
            contactsListAdapter.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, final int position, boolean isLongClick) {
                    if (!isLongClick) {
                        Toast.makeText(getActivity(), "TODO Show contact", Toast.LENGTH_SHORT).show();
                    } else {
                        final BottomDialogOptions dialog = new BottomDialogOptions(getActivity(), true);
                        dialog.show();

                        dialog.getDelete().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SQLiteCursorWrapper.getInstance(getActivity()).deleteContact(contacts.get(position).getId());
                                contactsListAdapter.delete(position);
                                dialog.dismiss();
                            }
                        });
                    }
                }
            });

            contactsRecyclerView.setAdapter(contactsListAdapter);
        }
    }
}
