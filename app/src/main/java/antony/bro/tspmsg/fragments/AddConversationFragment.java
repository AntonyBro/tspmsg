package antony.bro.tspmsg.fragments;


import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import antony.bro.tspmsg.MainActivity;
import antony.bro.tspmsg.R;
import antony.bro.tspmsg.adapters.ContactsAdapter;
import antony.bro.tspmsg.adapters.ItemClickListener;
import antony.bro.tspmsg.sql.Contact;
import antony.bro.tspmsg.sql.Conversation;
import antony.bro.tspmsg.sql.SQLiteCursorWrapper;

import java.util.ArrayList;
import java.util.List;

public class AddConversationFragment extends Fragment {

    public static final String EXTRA_ADD_CONVERSATION = "add_conversation";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_conversation, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.add_conversation);

        View view = getView();
        if (view != null) {

            final List<Contact> contacts = SQLiteCursorWrapper.getInstance(getActivity()).getAllContacts();
            if (contacts == null)
                return;

            final List<Conversation> conversations = SQLiteCursorWrapper.getInstance(getActivity()).getAllConversations();
            final List<Long> filterContacts = new ArrayList<>();

            if (conversations != null) {
                for (Conversation conversation : conversations)
                    filterContacts.add(conversation.getContactId());
            }

            final List<Contact> filteredContacts = new ArrayList<>();

            for (Contact contact : contacts) {
                if (!filterContacts.contains(contact.getId())) {
                    filteredContacts.add(contact);
                }
            }

            GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);

            final RecyclerView contactsList = (RecyclerView) view.findViewById(R.id.add_conversation_contacts_list);
            contactsList.setLayoutManager(layoutManager);

            ContactsAdapter contactsAdapter = new ContactsAdapter(filteredContacts);
            contactsAdapter.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    MessageFragment fragment = new MessageFragment();
                    fragment.setIsCreateNewConversation(true);
                    Bundle bundle = new Bundle();
                    bundle.putString(MessageFragment.EXTRA_CONTACT_NAME, filteredContacts.get(position).getName());
                    bundle.putLong(MessageFragment.EXTRA_CONTACT_ID, filteredContacts.get(position).getId());
                    fragment.setArguments(bundle);

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left,
                            R.anim.enter_from_left, R.anim.exit_to_right);
                    ft.replace(R.id.content_frame, fragment);
                    ft.addToBackStack(EXTRA_ADD_CONVERSATION);
                    ft.commit();
                }
            });

            contactsList.setAdapter(contactsAdapter);
        }
    }
}
