package antony.bro.tspmsg.fragments;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import antony.bro.tspmsg.AsyncTaskURL;
import antony.bro.tspmsg.MainActivity;
import antony.bro.tspmsg.R;
import antony.bro.tspmsg.adapters.ItemClickListener;
import antony.bro.tspmsg.adapters.MessageAdapter;
import antony.bro.tspmsg.sql.Contact;
import antony.bro.tspmsg.sql.Message;
import antony.bro.tspmsg.sql.SQLiteCursorWrapper;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageFragment extends Fragment {

    public static final String EXTRA_CONTACT_ID = "contact_id";
    public static final String EXTRA_CONVERSATION_ID = "conversation_id";
    public static final String EXTRA_CONTACT_NAME = "name";

    private boolean mIsCreateNewConversation;
    private long mConversationId;
    private EditText mMessage;
    private ListView mMessagesContainer;
    private MessageAdapter mAdapter;
    private Handler mTimerHandler;
    private Runnable mTimerRunnable;

    public void setIsCreateNewConversation(boolean isCreateNewConversation) {
        this.mIsCreateNewConversation = isCreateNewConversation;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        View view = getView();
        if (view != null) {

            mMessagesContainer = (ListView) view.findViewById(R.id.messagesContainer);
            mMessage = (EditText) view.findViewById(R.id.messageEdit);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mMessage.setBackground(null);
            }
            ImageButton sendBtn = (ImageButton) view.findViewById(R.id.sendBtn);

            mAdapter = new MessageAdapter(getActivity(), new ArrayList<Message>());
            mMessagesContainer.setAdapter(mAdapter);

            mAdapter.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (!isLongClick) {
                        Toast.makeText(getContext(), "Click", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), "Long click", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            if (mIsCreateNewConversation) {
                ((MainActivity) getActivity()).getSupportActionBar().setTitle(getArguments().getString(EXTRA_CONTACT_NAME));
            } else {
                mConversationId = getArguments().getLong(EXTRA_CONVERSATION_ID);
                loadHistory(mConversationId);
            }

            final Contact contact = SQLiteCursorWrapper.getInstance(getActivity()).getMyProfile();

            sendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String messageText = mMessage.getText().toString();
                    if (TextUtils.isEmpty(messageText)) {
                        return;
                    }

                    final Message outMessage = new Message(true, messageText, DateFormat.getDateTimeInstance().format(new Date()), mConversationId);
                    mMessage.setText("");

                    if (mIsCreateNewConversation) {
                        mConversationId = SQLiteCursorWrapper.getInstance(getActivity()).
                                createConversation(getArguments().getString(EXTRA_CONTACT_NAME),
                                        getArguments().getLong(EXTRA_CONTACT_ID));

                        mIsCreateNewConversation = false;
                    }

                    final String toEmail = SQLiteCursorWrapper.getInstance(getActivity()).getContactByConversationId(mConversationId).getEmail();

                    AsyncTaskURL asyncTaskURL = new AsyncTaskURL("http://128.199.52.224:9000/set/message?from=" +
                            contact.getEmail() + "&to=" + toEmail + "&msg=" + messageText);

                    asyncTaskURL.setAsyncResponseHandler(new AsyncTaskURL.AsyncResponseHandler() {
                        @Override
                        public void processFinish(String json) {
                            if (json.equals("1")) {
                                SQLiteCursorWrapper.getInstance(getActivity()).
                                        insertMessage(outMessage.isMe(), outMessage.getMessage(), outMessage.getDateTime(), mConversationId);
                                displayMessage(outMessage);
                            }
                        }
                    });

                    asyncTaskURL.execute((Void) null);
                }
            });

            mTimerHandler = new Handler();
            mTimerRunnable = new Runnable() {

                @Override
                public void run() {
                    AsyncTaskURL asyncTaskURL = new AsyncTaskURL("http://128.199.52.224:9000/get/message?email=" + contact.getEmail());

                    asyncTaskURL.setAsyncResponseHandler(new AsyncTaskURL.AsyncResponseHandler() {
                        @Override
                        public void processFinish(String json) {
                            if (!json.equals("0")) {
                                Log.i("Message:", json);
                                JSONArray jsonArray;
                                try {
                                    jsonArray = new JSONArray(json);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        final Message outMessage = new Message(false, jsonArray.getJSONObject(i).getString("Message"), DateFormat.getDateTimeInstance().format(new Date()), mConversationId);
                                        SQLiteCursorWrapper.getInstance(getActivity()).
                                                insertMessage(outMessage.isMe(), outMessage.getMessage(), outMessage.getDateTime(), mConversationId);
                                        displayMessage(outMessage);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Log.i("Message:", json);
                            }
                        }
                    });

                    asyncTaskURL.execute((Void) null);

                    mTimerHandler.postDelayed(this, 3000);
                }
            };

            mTimerHandler.postDelayed(mTimerRunnable, 0);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mTimerHandler.removeCallbacks(mTimerRunnable);
    }

    private void displayMessage(Message message) {
        mAdapter.add(message);
        mAdapter.notifyDataSetChanged();

        mMessagesContainer.setSelection(mMessagesContainer.getCount() - 1);
    }

    private void loadHistory(long conversationId) {
        List<Message> chatHistory = SQLiteCursorWrapper.getInstance(getActivity()).getAllMessages(conversationId);

        if (chatHistory != null) {
            for (int i = 0; i < chatHistory.size(); i++) {
                Message message = chatHistory.get(i);
                displayMessage(message);
            }
        }
    }
}
