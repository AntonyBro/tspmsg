package antony.bro.tspmsg.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;

import antony.bro.tspmsg.AsyncTaskURL;
import antony.bro.tspmsg.MainActivity;
import antony.bro.tspmsg.R;
import antony.bro.tspmsg.adapters.ItemClickListener;
import antony.bro.tspmsg.adapters.SearchAdapter;
import antony.bro.tspmsg.sql.Contact;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {

    private List<Contact> mContacts;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(R.string.search_contacts);

        View view = getView();
        if (view != null) {
            mContacts = new ArrayList<>();
            LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

            final RecyclerView contactsRecyclerView = (RecyclerView) getView().findViewById(R.id.search_list);
            final ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);
            contactsRecyclerView.setLayoutManager(layoutManager);

            final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            final SearchAdapter mSearchAdapter = new SearchAdapter();
            mSearchAdapter.setContacts(mContacts);
            mSearchAdapter.setItemClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, final int position, boolean isLongClick) {
                    if (!isLongClick) {
                        Bundle bundle = new Bundle();
                        bundle.putString(ContactsFragment.EXTRA_NAME, mContacts.get(position).getName());
                        bundle.putString(ContactsFragment.EXTRA_EMAIL, mContacts.get(position).getEmail());

                        ContactsFragment contactsFragment = new ContactsFragment();
                        contactsFragment.setArguments(bundle);

                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                                R.anim.enter_from_right, R.anim.exit_to_left);
                        ft.replace(R.id.content_frame, contactsFragment);

                        ft.commit();
                    }
                }
            });

            contactsRecyclerView.setAdapter(mSearchAdapter);

            EditText search = (EditText) getView().findViewById(R.id.search);

            search.requestFocus();
            inputMethodManager.showSoftInput(search, InputMethodManager.SHOW_IMPLICIT);

            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    contactsRecyclerView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() != 0) {

                        AsyncTaskURL asyncTaskURL = new AsyncTaskURL();
                        asyncTaskURL.setUrl("http://128.199.52.224:9000/search/user?name=" + s);
                        asyncTaskURL.setAsyncResponseHandler(new AsyncTaskURL.AsyncResponseHandler() {
                            @Override
                            public void processFinish(String json) {
                                if (!json.equals("0")) {
                                    Log.wtf("Search contact:", json);
                                    JSONArray jsonArray;
                                    try {
                                        jsonArray = new JSONArray(json);
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            mContacts.add(new Contact(
                                                    0,
                                                    jsonArray.getJSONObject(i).getString("Email"),
                                                    jsonArray.getJSONObject(i).getString("Name")));
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mSearchAdapter.notifyItemInserted(mContacts.size() - 1);
                                    mSearchAdapter.notifyDataSetChanged();
                                    contactsRecyclerView.setVisibility(View.VISIBLE);
                                    progressBar.setVisibility(View.GONE);
                                } else {
                                    mSearchAdapter.clearData();
                                    progressBar.setVisibility(View.GONE);
                                }
                            }
                        });

                        asyncTaskURL.execute((Void) null);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }

            });
        }
    }
}
